# awesome-product-management
## by Brian Piercy, bjpcjp@gmail.com

### Advertising, Copywriting, Content

[31 Copywriting tips (Nick Kolenda)](https://www.nickkolenda.com/copywriting-tips/)

[Viral Headline Keywords (Buffer)](https://blog.bufferapp.com/the-most-popular-words-in-most-viral-headlines)

[Creating Content Ideas When You Don't Have a Clue (Copyblogger)](http://www.copyblogger.com/create-content-ideas/)

[The Destructive Power of Adjectives (Kissmetrics)](https://blog.kissmetrics.com/destructive-power-of-adjectives/)

[How Hollywood Does Writing](http://www.bakadesuyo.com/2015/07/how-to-improve-your-writing/)

[Creating Advertising Copy that Sells (David Ogilvy)](https://www.farnamstreetblog.com/2011/09/create-advertising-that-sells/)

[Why You Should Write Your Own Product Descriptions (eCreative Works)](http://www.ecreativeworks.com/blog/2015/04/07/why-you-should-write-your-own-product-descriptions/)

[Words to Avoid in Sales Situations (Insight Squared)](http://www.insightsquared.com/2013/07/13-words-you-need-to-avoid-in-sales)

[Attention-Grabbing Cover Letters (The Muse)](https://www.themuse.com/advice/31-attentiongrabbing-cover-letter-examples)

### Analytics

[5 Tricks When You Can't Use A/B (Medium)](https://medium.com/teconomics-blog/5-tricks-when-ab-testing-is-off-the-table-f2637e9f15a5)

[A/B vs Multivariate Testing (NN Group)](https://www.nngroup.com/articles/multivariate-testing/)

[A/B and Pricing Pages (Price Intelligently)](http://www.priceintelligently.com/blog/ab-test-pricing-page-strategy)

[12 Common Metrics Interpretaton Mistakes (Acolyer)](https://blog.acolyer.org/2017/09/25/a-dirty-dozen-twelve-common-metric-interpretation-pitfalls-in-online-controlled-experiments/)

statistical signficance calculator:
https://www.dropbox.com/s/qfvnedbunv7xbt6/statistical_significance_calculator.xlsx?dl=0

__Metrics Definitions (A16Z)__: [Part I](http://a16z.com/2015/08/21/16-metrics/), [Part II](http://a16z.com/2015/09/23/16-more-metrics/)

[A/B Testing: Scale Cheat Sheet (Ben Tilly)](http://bentilly.blogspot.com/2012/10/ab-testing-scale-cheat-sheet.html)

[Testing Should Yield More Than Result (Intercom)](https://blog.intercom.com/why-ab-tests-should-yield-more-than-results/)

https://blog.twitter.com/2016/implications-of-use-of-multiple-controls-in-an-ab-test

https://searchenginewatch.com/sew/how-to/2223888/23-tips-on-how-to-a-b-test-like-a-badass

[8 Rules of A/B Testing (Search Engine Watch)](https://searchenginewatch.com/sew/how-to/2196754/8-rules-of-a-b-testing-the-art-in-marketing-science)

[26 Ideas for Split Testing Your Search Ads (Search Engine Land)](http://searchengineland.com/infographic-26-ideas-for-split-testing-your-search-ads-155821)

http://www.evanmiller.org/sequential-ab-testing.html
http://www.evanmiller.org/ab-testing/

https://blog.crazyegg.com/2014/12/30/ab-testing-rules

https://blog.kissmetrics.com/ab-tests-shocking-discoveries/
https://blog.kissmetrics.com/know-about-ab-testing/
https://blog.kissmetrics.com/post-conversion-questionnaires/

http://varianceexplained.org/r/bayesian_ab_baseball/

https://blog.asmartbear.com/easy-statistics-for-adwords-ab-testing-and-hamsters.html

https://productcoalition.com/start-here-statistics-for-a-b-testing-5f5c7e02ce1e#.jk219vqlm

https://blog.dominodatalab.com/ab-testing-with-hierarchical-models-in-python/

https://www.conversioner.com/ab-test-calculator

http://larslofgren.com/growth/7-rules-for-ab-testing

https://vwo.com/blog/increase-relevance-to-boost-conversions/



### Auctions

### Branding, Naming, Trademarks & Patents

http://www.trademarkia.com/

https://namechk.com/

https://www.brandbucket.com/trackchip

https://www.nytimes.com/2015/01/18/magazine/the-weird-science-of-naming-new-products.html?_r=0

http://searchengineland.com/the-complete-guide-to-bidding-on-competitor-brand-names-trademarked-terms-118576

https://techcrunch.com/2012/11/10/an-entrepreneurs-guide-to-patents-the-basics/

http://www.trademarkia.com/register-trademark/grey/US/trademark_services.htm

https://search.uspto.gov/search

https://hbr.org/2014/02/protect-your-brand-from-cybersquatting

http://www.cognatarium.com/cognatarium/

https://www.slideshare.net/rcauvin/searchcom-or-why-we-suck-at-naming-products-and-companies

mattress industry
https://www.curbed.com/2018/3/28/17164898/bed-in-a-box-online-mattress-brands-why-so-many

https://designschool.canva.com/wp-content/uploads/sites/2/2015/04/How-To-Design-a-Memorable-Brand-That-Catches-On.pdf

http://www.xconomy.com/san-francisco/2012/08/28/whats-a-brand-anyway-the-story-of-nuts-com

https://techcrunch.com/2012/05/06/startups-live-die-by-these-5-street-smart-laws-of-advertising/

http://webdesignfromscratch.com/digital-strategy/pure-gold-positioning-questionnaire

http://www.gkogan.co/blog/ugly-ad-saved-business/

behavior advertising
https://blog.kissmetrics.com/behavioral-advertising/

### Catalogs & Menus

[Restaurant Menu Tricks (BBC)](http://www.bbc.com/future/story/20171120-the-secret-tricks-hidden-inside-restaurant-menus)

https://www.theatlantic.com/technology/archive/2014/03/the-engineering-of-the-chain-restaurant-menu/284371/

http://www.neurosciencemarketing.com/blog/articles/neuro-menus-and-restaurant-psychology.htm

[Pricing & Menu Engineering](https://gigaom.com/2009/09/13/what-we-can-learn-about-pricing-from-menu-engineers/)

### Conversions & Friction

https://medium.com/@sarahtavel/the-hierarchy-of-engagement-expanded-648329d60804

http://firstround.com/review/amazons-friction-killing-tactics-to-make-products-more-seamless/

Traction ebook
https://www.dropbox.com/s/tudibdion9p6grt/Traction%20A%20Startup%20Guide.pdf?dl=0

http://firstround.com/review/How-Lumosity-Spiked-Active-Users-10-with-Complexity-Not-Simplicity/

https://www.thumbtack.com/engineering/the-usability-experts-were-right-5-changes-we-made-to-increase-conversion-by-50/

https://hackpad.com/Traction-Book-Notes-j19XmQsaBZ4

https://medium.com/platform-thinking/how-paypal-and-reddit-faked-their-way-to-traction-9411fb583205#.u152e5g9i

http://uxmovement.com/forms/8-reasons-users-arent-filling-out-your-sign-up-form/

### Culture & Leadership

https://www.forbes.com/sites/erikaandersen/2012/08/27/why-some-companies-lose-their-best-people-and-others-dont/#14e4d2b77b47

https://www.mojotech.com/blog/startup-culture-is-not-about-ping-pong-tables/

http://philip.yurchuk.com/software/buying-enterprise-software/

Marissa Mayer at Yahoo:
https://hackernoon.com/what-marissa-mayer-brought-to-yahoo-that-cant-be-bought-or-sold-4ee82382e4ee

Valve's new employee handbook
http://www.valvesoftware.com/company/Valve_Handbook_LowRes.pdf

Principles by Ray Dalio, Bridgewater
https://inside.bwater.com/publications/principles_excerpt

https://www.quora.com/What-makes-someone-a-great-product-manager-at-Google-What-are-the-necessary-and-ideal-qualities-to-have/answer/Rob-Ennals

Price
https://www.strategy-business.com/blog/How-to-Harness-Employees-Emotional-Energy 

https://www.leadershipnow.com/leadingblog/2018/01/8_principles_for_building_a_hi.html

http://www.thagomizer.com/blog/2017/09/29/we-don-t-do-that-here.html

44 engineering management lessons
http://www.defmacro.org/2014/10/03/engman.html

Psychological Safety, Risk Tolerance and High Functioning Software Teams
https://hackernoon.com/psychological-safety-risk-tolerance-and-high-functioning-software-teams-75701ed23f68

42 Rules to Lead by from the Man Who Defined Google's Product Strategy
http://firstround.com/review/42-Rules-to-Lead-by-from-the-Man-Who-Defined-Googles-Product-Strategy/

https://www.blossom.co/blog/building-a-strong-product-culture

Lessons learned from scaling a product team
https://blog.intercom.com/how-we-build-software/


http://www.skmurphy.com/blog/2016/09/07/constructive-pessimism/

https://blog.adamnash.com/2012/03/06/top-10-product-leadership-lessons/

https://medium.com/@gerstenzang/21-management-things-i-learned-at-imgur-7abb72bdf8bf#.aegzrien2

http://tomtunguz.com/management-best-practice-1/

http://jvns.ca/blog/2016/04/30/building-expertise-at-work/

http://blog.mojotech.com/startup-culture-is-not-about-ping-pong-tables/

https://blog.adamnash.com/2010/11/29/why-t-shirts-matter/

http://www.slideshare.net/reed2001/culture-1798664 (netflix culture deck)

### Checklists

https://www.sideprojectchecklist.com/marketing-checklist

http://nautil.us/blog/the-tricks-used-by-pilots-surgeons--engineers-to-overcome-human-error

https://www.scotthyoung.com/blog/2011/01/11/learn-faster-and-better/

http://www.newyorker.com/magazine/2013/07/29/slow-ideas

### Curation & Personalization

[Curated Consumer Markets: 10 Success Criteria (Forbes)](https://www.forbes.com/sites/ciocentral/2013/10/03/the-new-curated-consumer-marketplace-model-10-criteria-for-success/#3295f04e650c)

[Content Curation Tools (Practical eCommerce)](http://www.practicalecommerce.com/articles/88111-15-Tools-to-Curate-Content-for-Social-Media-Newsletters-More)

[Curation & Algorithms (Stratechery)](https://stratechery.com/2015/curation-and-algorithms/)

[Personalization is NOT a Feature (TechCrunch)](https://techcrunch.com/2012/05/18/personalization-is-not-a-feature)

[Algorithmic Personalization (TechCrunch)](https://techcrunch.com/2015/06/25/the-future-of-algorithmic-personalization/)

[Past Behavior does NOT determine Future Purchases (TechCrunch)](https://techcrunch.com/2015/10/24/past-behavior-does-not-determine-future-purchases)

[latforms to Build User-Generated Content](http://www.practicalecommerce.com/articles/94621-20-Platforms-to-Build-Dynamic-User-generated-Content)

http://mashable.com/2013/08/20/republic-spaces

[Curation & Music Labels (Recode)](http://www.recode.net/2015/7/23/11615008/guess-whos-making-a-comeback-record-labels)

[Crowd Patronage (Bryank.im)](http://bryank.im/crowdpatronage)

[Human Curation Makes a Comeback (Monday Note)](https://mondaynote.com/human-curation-is-back-f32bc0ccc2aa#.ivi6zijq2)

### Dashboards

http://blog.outlyer.com/2015/03/18/using-monitoring-dashboards-to-change-behaviour/

### Decision Making

[Deciding What to Build (DC Gross)](https://dcgross.com/decide-what-to-build/)

[Finding Winning Ideas - With a Confidence Tool (HackerNoon)](https://hackernoon.com/finding-winning-ideas-using-the-confidence-tool-d8f2d8cc2c15)

[The Cost per Reasonable Decision (John Cutler)](https://medium.com/@johnpcutler/cost-per-reasonable-decision-cprd-ed1ca8c6147)

[Product Prioritization Techniques (Folding Burritos)](https://foldingburritos.com/product-prioritization-techniques/)

http://changingminds.org/explanations/theories/a_decision.htm

http://nautil.us/issue/52/the-hive/how-to-choose-wisely-rp
http://nautil.us/issue/41/selection/how-to-choose-wisely

http://firstround.com/review/square-defangs-difficult-decisions-with-this-system-heres-how/?ct=t(How_Does_Your_Leadership_Team_Rate_12_3_2015)

https://medium.com/lessons-from-mckinsey/making-decisions-under-uncertainty-c1d1dfbb02b2#.nosac75uq

https://www.farnamstreetblog.com/smart-decisions/

https://blackboxofpm.com/making-good-decisions-as-a-product-manager-c66ddacc9e2b

https://www.farnamstreetblog.com/smart-decisions/

http://ideas.ted.com/how-cultures-around-the-world-make-decisions/

https://www.farnamstreetblog.com/2015/02/seymour-schulich-the-decision-maker/

http://hbswk.hbs.edu/item/how-our-brain-determines-if-the-product-is-worth-the-price

https://hbr.org/2015/11/what-really-makes-customers-buy-a-product

http://www.sachinrekhi.com/the-art-of-decision-making-as-a-product-manager

https://medium.com/product-labs/6-decision-making-techniques-all-product-managers-should-know-429fe4d13654#.egpho6ooz (decision techniques)

https://www.1843magazine.com/features/the-data-or-the-hunch?page=full

https://www.quantamagazine.org/20160823-the-neuroscience-behind-bad-decisions/

### Design

(wireframing)
https://www.pinterest.com/wireframes/great-wireframe-examples/

### Discovery / Customer Interviewing

http://purde.net/2018/02/customer-development-pipedrive/

Talking to Humans
http://www.talkingtohumans.com/index

Feature Requests
https://m.signalvnoise.com/a-new-approach-to-feature-requests-21bea562c083

Why People Cancel
http://www.extendslogic.com/business/why-people-cancel/

How to get meetings with people too busy to meet with you
https://steveblank.com/2013/08/12/how-to-get-meetings-with-people-too-busy-to-see-you

Wide|Narrow problem view vs Affirm|Discovery question intent
https://hbr.org/2015/03/relearning-the-art-of-asking-questions?

http://tomtunguz.com/price-discovery/

http://www.skmurphy.com/blog/2014/11/24/its-ok-to-ask-would-you-use-this-in-customer-discovery/

http://www.skmurphy.com/blog/2014/11/21/customer-interviews-how-to-organize-findings

The Mom Test:
https://www.dropbox.com/s/rxlvnfflt651cm5/The%20Mom%20Test%20by%20%40robfitz%20-%20A5.pdf?dl=0

http://firstround.com/review/the-power-of-interviewing-customers-the-right-way-from-twitters-ex-vp-product/

https://steveblank.com/2014/02/19/how-to-be-smarter-than-your-investors-continuous-customer-discovery/

http://kevindewalt.com/2013/02/09/how-i-make-customer-development-interviews-less-weird-and-more-natural/

https://www.fastcompany.com/3003945/one-conversational-tool-will-make-you-better-absolutely-everything

http://www.futurelab.net/blog/2014/04/maybe-voice-customer-isn%E2%80%99t

Resources
https://blog.kissmetrics.com/26-customer-development-resources/

Questions to ask
http://mfishbein.com/the-ultimate-list-of-customer-development-questions/

Non-customer interviews
https://www.siriusdecisions.com/Blog/2015/March/Identifying-NonCustomers-for-Customer-Interviews.aspx

https://www.quora.com/What-are-some-methods-and-tools-for-analyzing-customer-discovery-interviews

### Failure & Postmortems

http://informationarbitrage.com/post/2953984660/failing-well

http://www.chubbybrain.com/blog/startup-failure-post-mortem/

http://autopsy.io/

https://steveblank.com/2015/03/11/fear-of-failure-and-lack-of-speed-in-a-large-corporation/

https://www.slideshare.net/danmil30/how-to-run-a-5-whys-with-humans-not-robots

https://pando.com/2012/11/24/the-big-takeaway-learning-from-failure/

http://www.mckinsey.com/business-functions/strategy-and-corporate-finance/our-insights/learning-to-let-go-making-better-exit-decisions

http://www.vox.com/2015/11/20/9757186/netflix-video-rental-store

what is an 8D?
https://en.wikipedia.org/wiki/Eight_Disciplines_Problem_Solving

what is an FMEA?
https://en.wikipedia.org/wiki/Failure_mode_and_effects_analysis

### Finance

http://www.slideshare.net/evenanerd/understanding-financial-statements

http://www.goldmansachs.com/s/interactive-guide-to-capital-markets/?cid=igtcm2

https://hbr.org/2015/07/a-refresher-on-debt-to-equity-ratio

https://hbr.org/2015/04/a-refresher-on-cost-of-capital

product cost calculator (chip centric)
https://www.dropbox.com/s/vlx85swe4nyqbmh/chip%20cost%20calculator.xls?dl=0

http://www.investopedia.com/university/futures/

SG&A metrics grouped by industry
https://saibooks.com/index.php?option=com_content&view=article&id=64&Itemid=61

2015 valuation handbook
https://books.google.com/books?id=eC-9BgAAQBAJ&pg=SL3-PA94&dq=GSI+Technology&client=internal-uds&num=4&cd=4&source=uds#v=onepage&q=GSI%20Technology&f=false

### Focus & Execution

http://limedaring.com/articles/how-i-run-a-marketplace-with-eleven-different-properties-and-5000-vendors/

http://sethgodin.typepad.com/seths_blog/2015/05/how-to-go-faster.html

90 things from 4 companies
http://mashable.com/2012/10/05/90-entrepreneurship-lessons-fab/#OWHYOAYlbZqs

https://hbr.org/2015/11/two-things-to-do-after-every-meeting

better meetings:
https://foldingburritos.com/articles/2018/02/13/report-templates-less-status-meetings/

project management:
https://www.joelonsoftware.com/2007/10/26/evidence-based-scheduling/

best practices
http://a16z.com/2016/03/25/building-weatherproof-companies/

best practices - code
https://segment.com/blog/engineering-best-practices/

behavior
http://jvns.ca/blog/2016/09/19/getting-things-done/

the Cost of a Screw and the Value of a Photo
http://tomtunguz.com/musk/

behaviors
http://blog.samaltman.com/super-successful-companies

radical focus
https://www.dropbox.com/s/ny5jl49zqm94r9p/Radical%20Focus.docx?dl=0

objectives & key results
http://eleganthack.com/the-art-of-the-okr/

http://eleganthack.com/why-key-results-need-to-be-results/

http://firstround.com/review/make-operations-your-secret-weapon-heres-how/

http://www.terrystarbucker.com/2012/01/15/execution-leadership-12-metrics-that-must-be-measured-monitored-and-managed-relentlessly/

http://www.mckinsey.com/industries/high-tech/our-insights/moneyball-for-engineers-what-the-semiconductor-industry-can-learn-from-sports

http://www.helloerik.com/bump-the-lamp-the-reason-for-caring

http://www.inc.com/magazine/201402/leigh-buchanan/business-lessons-from-german-mittelstand.html

http://pmhardcore.com/2013/09/25/ten-tactics-to-do-the-impossible/

https://blog.asmartbear.com/startup-business-plan.html

https://bothsidesofthetable.com/startup-advice-1272d797eb07#.1lzfouyc5

57 startup lessons
http://www.defmacro.org/2013/07/23/startup-lessons.html
> People
> Fundraising
> Markets
> Products
> Marketing
> Sales
> Development
> Admin
> Well Being

https://martinfowler.com/articles/products-over-projects.html

https://www.joelonsoftware.com/2000/08/09/the-joel-test-12-steps-to-better-code/

### Gamification

https://webdesign.tutsplus.com/articles/the-benefits-and-pitfalls-of-gamification--webdesign-6454

http://www.socialmediaexaminer.com/26-elements-of-a-gamification-marketing-strategy/

### Growth Hacks & Virality

https://johnmcelborough.com/growth-hacking-strategies/

### Habits

http://cmxhub.com/growthhackers-hooked-retention/

http://www.nirandfar.com/2012/09/habit-zone.html

http://www.nirandfar.com/2014/10/ryan-holiday.html

http://jamesclear.com/habit-guide

https://zenhabits.net/ship/

https://www.farnamstreetblog.com/2014/08/habit-stacking/

https://techcrunch.com/2012/02/26/habits-are-the-new-viral-why-startups-must-be-behavior-experts/

### Ideation

https://hackernoon.com/finding-winning-ideas-using-the-confidence-tool-d8f2d8cc2c15

https://designschool.canva.com/blog/13-ways-generate-new-ideas/
https://www.smithsonianmag.com/innovation/where-do-new-ideas-come-from-180965202/

http://sethgodin.typepad.com/seths_blog/2017/04/the-bingo-method.html

https://docs.google.com/spreadsheets/d/1xafwdQyWczCwPDGsGoiCSe8SdlbgN-QHjktLemqRo7s/edit

Deliberate Ideation
https://www.quora.com/What-are-the-best-ways-to-think-of-ideas-for-a-startup
 (spreadsheet, dropbox):
https://www.dropbox.com/s/1kg72cs7rtagb15/Deliberate%20ideation.xlsx?dl=0

http://paulgraham.com/startupideas.html

http://www.newyorker.com/science/maria-konnikova/where-do-eureka-moments-come-from

http://www.startuprob.com/23-ways-to-generate-startup-ideas/

http://diytoolkit.org/tools/fast-idea-generator-2/ (dropbox: https://www.dropbox.com/s/zsdzw7l87pyxugc/Idea-Generator.pdf?dl=0)

http://diytoolkit.org/media/Fast-Idea-Generator-Size-A4.pdf (dropbox: https://www.dropbox.com/s/pqi35sdpg8c6kcy/Idea-Generator-Size-A4.pdf?dl=0)

http://eleganthack.com/one-big-idea/

http://eleganthack.com/ideation-sprints-for-new-products-services/ (dropbox: https://www.dropbox.com/sh/jl0d0seu4emjbtw/AACq_yERahMQMZw9TrX-8kYna?dl=0)

https://hbr.org/2014/06/collective-genius

http://www.spring.org.uk/2010/03/boost-creativity-7-unusual-psychological-techniques.php

http://sethgodin.typepad.com/seths_blog/2013/01/understanding-idea-diffusion.html

http://www.technovelgy.com/ct/ctnlistalpha.asp (Glossary of Science Fiction Ideas, Technology and Inventions)

https://www.wired.com/2013/05/one-click-selling/ (Why 3 MIT Grads Want to Send You an Empty Box)

https://blog.asmartbear.com/your-idea-sucks-now-go-do-it-anyway.html

### Infomercials

https://medium.com/the-mission/learn-buyer-psychology-66b969578b2b

### Job Definition

https://twitter.com/noah_weiss/status/999311514556284928

https://hbr.org/2017/12/what-it-takes-to-become-a-great-product-manager

(polymaths)
http://99u.com/articles/7269/Picasso-Kepler-and-the-Benefits-of-Being-an-Expert-Generalist

(saying no)
https://generalassemb.ly/blog/how-to-say-no-to-your-ceo/
https://blog.intercom.com/product-strategy-means-saying-no/

(street smarts)
https://medium.com/@brandonmchu/product-management-is-a-lot-like-playing-poker-a1f3b00dcbe#.mf1ezuq3g

https://iterativepath.wordpress.com/2013/09/28/dont-be-a-product-person-be-a-merchant/

(PM Handbook)
https://www.dropbox.com/s/uw4o6lwsrj333dv/the-product-manager-handbook.pdf?dl=0

(top 1%)
https://www.quora.com/What-distinguishes-the-Top-1-of-Product-Managers-from-the-Top-10

https://www.kennorton.com/essays/productmanager.html
https://www.kennorton.com/essays/leading-cross-functional-teams.html

http://svpg.com/behind-every-great-product/

https://www.usersknow.com/blog/

https://www.quora.com/Product-Management/What-distinguishes-the-Top-1-of-Product-Managers-from-the-Top-10

(culture)
http://www.hightechinthehub.com/2013/03/product-management-is-a-company-not-a-department/

https://www.linkedin.com/pulse/four-types-product-management-skills-steve-johnson

http://pmhardcore.com/2015/04/13/the-astonishing-financial-benefits-of-improving-pm-effectiveness/

(interviews):
https://www.dropbox.com/s/mbhjw819nh5sjtn/Cracking%20the%20PM%20Interview.epub?dl=0

https://medium.com/@joshelman/a-product-managers-job-63c09a43d0ec#.c7xdoxskp

https://medium.com/evergreen-business-weekly/how-to-master-the-discipline-of-product-management-not-the-job-of-product-manager-28d2c493d445#.ed6hym9as

### Lead Generation, Lead Scoring

[__Hubspot Guide__](https://offers.hubspot.com/hs-fs/hub/53/file-230749144-pdf/offers/An-Introduction-to-Lead-Generation.pdf)

https://mattermark.com/effective-lead-scoring-includes-company-data/

http://www.marketingsherpa.com/article/case-study/auto-qualify-new-leads-without-lead-scoring

https://medium.com/the-mission/5-lesser-known-lead-generation-hacks-e2f10d9a9784


### Lean, Scrum, Kanban

* [Leanstack](https://leanstack.com)

### Onboarding

http://boz.com/articles/career-cold-start.html

• 1: (25 minutes) ask interviewee to tell you everything they think you should know. take tons of notes. only stop them to ask about things you don't understand.
• 2: (3 minutes) ask about the biggest challenges the team has right now.
• 3 (2 minutes) ask who else you should talk to. write down every name.

### Packaging

http://www.adweek.com/brand-marketing/how-package-designers-use-science-influence-your-subconscious-mind-158319/

http://gorillastudio.co.uk/branding/packaging-design-five-simple-rules-designing-perfect-packaging/

https://www.shopify.com/blog/18989252-5-ways-to-use-packaging-inserts-to-increase-customer-loyalty-and-revenue

http://theaccidentalpm.com/z-product/a-candy-bar-teaches-product-managers-lessons-about-redesigning-product-packaging

https://www.fastcodesign.com/1671614/branding-a-gentlemens-subscription-service-with-old-school-charm

https://blog.kissmetrics.com/custom-shipping-supplies/

### Pitching & Presentations

[https://getpocket.com/explore/item/the-greatest-sales-deck-i-ve-ever-seen-1414678490]

https://medium.com/the-mission/the-best-sales-pitch-ive-seen-all-year-7fa92afaa248

https://medium.com/the-mission/the-greatest-sales-deck-ive-ever-seen-4f4ef3391ba0#.oigbovgec

### Pivots

https://blog.asmartbear.com/lean-pivot.html

http://www.instigatorblog.com/experimenting-with-product-price-and-market/2015/02/09/

https://blog.asmartbear.com/unsustainable-companies.html

http://kevindewalt.com/2012/11/24/what-if-you-cant-find-customers-to-develop/

https://hbr.org/2012/09/too-many-pivots-too-little-passion

https://steveblank.com/2014/01/14/whats-a-pivot/

https://www.theatlantic.com/technology/archive/2014/01/how-netflix-reverse-engineered-hollywood/282679/

### Platforms & Marketplaces

PDF on file in Gdrive: https://drive.google.com/open?id=1fZBjApVhYOTRbsBrpj0LAXpJFF7u4B2c

1. Intro
   > what's in a market?
      - aggregation of sellers & inventory;  transaction enabler

   > classifications?
      - P2P, B2C, B2B

   > most common metrics?
      - GMV (gross merch value: how much is sold per time pd)
      - Take Rate (%fee)

   > key factors for success? (http://abovethecrowd.com/2012/11/13/all-markets-are-not-created-equal-10-factors-to-consider-when-evaluating-digital-marketplaces/)
      - high fragmentation
      - not much loyalty between buyers & sellers
      - higher frequency of purchases
      - total available market
      - being able to insert yourself into the payment flow
      - ability to expand market & create value
  
   > how to win against incumbents?
      - lower take rate
      - focus on best product for specific vertical
      - 10X better product
      - unique inventory

   > network effects?
      - direct
          > local (value-add may not be a function of total user base, but a subset)

      - indirect (ex: complementary goods & svcs)
      - network effects ARE NOT NECESSARILY EQUAL TO VIRALITY.

3. Seeding - how to do it?
   > identify unique inventory
   > convince existing sellers to list on your platform
   > bring customers to a (service) provider. (good way to build early liquidity)
   > pay for inventory
   > aggregate easily accessible inventory

4. Growing - how to do it?
   > double-down on id'd hot spots, a.k.a. "do things that don't scale."
   > be patient. marketplaces take time.

5. Scaling - key factors
   > promote sense of trust & safety (ratings, reviews, testimonials, guarantees)
   > support power sellers
   > develop an ecosystem
   > prevent off-platform "leakage"
   > build a moat: (ex: protect your supply; win buyer mindshare, ...)

6. Business Models
   > transaction fees
   > listing fees (limits upside, but encourages better quality)
   > seller services
   > What to do when services are delivered OFFLINE?
   > subscription fees
   > pricing strategy: a rake too far (http://abovethecrowd.com/2013/04/18/a-rake-too-far-optimal-platformpricing-strategy/)

9. New Marketplace Types
10. Metrics
11. Tools
12. Investors
13. Exits

http://cdixon.org/2011/02/05/selling-pickaxes-during-a-gold-rush/

https://thenextweb.com/insider/2012/12/22/reverse-network-effects-why-scale-may-be-the-biggest-threat-facing-todays-social-networks/#.tnw_Wfinkp7Y

https://hbr.org/2016/03/6-reasons-platforms-fail

http://platformed.info/platform-metrics/

http://platformed.info/platform-strategy-and-walled-gardens-in-the-toy-industry/

https://hbr.org/2016/04/network-effects-arent-enough

https://hbr.org/2016/09/the-businesses-that-platforms-are-actually-disrupting

https://hbr.org/2016/04/the-real-power-of-platforms-is-helping-people-self-organize

https://gist.github.com/ndarville/4295324 (list of business models)

http://hbswk.hbs.edu/item/three-dimensional-strategy-winning-the-multisided-platform

https://hbr.org/2016/03/everything-we-know-about-platforms-we-learned-from-medieval-france
https://hbr.org/2016/04/why-platform-disruption-is-so-much-bigger-than-product-disruption

http://platformed.info/seeding-platform-standalone-square-opentable-delicious/

http://platformed.info/creative-platform-threadless-500px-dribbble-instagram/

https://techcrunch.com/2012/08/19/how-to-structure-a-marketplace/

http://www.eugenewei.com/blog/2015/3/14/platform-risk

https://rishidean.com/2015/09/16/the-7-fundamental-marketplace-models

http://platformed.info/how-to-get-startup-ideas/

http://platformsandnetworks.blogspot.com/

http://abovethecrowd.com/2013/04/18/a-rake-too-far-optimal-platformpricing-strategy/

http://venturebeat.com/2015/03/21/how-to-protect-yourself-as-middleman-in-a-marketplace/

[3 Elements of a Successful Platform (HBR)](https://hbr.org/2013/01/three-elements-of-a-successful-platform)

https://medium.com/@jeffelder/in-times-of-change-make-tires-c8adfdd884cf#.o3p2eb99u

https://medium.com/@adambreckler/how-we-crack-the-chicken-and-the-egg-problem-eee25db9734c#.ghm8zkf03

[__Commoditize Your Compliment__:](https://www.gwern.net/Complement#2)

### Pricing

[Case Study: Shopping Guides & Order Value (Marketing Sherpa)](https://www.marketingsherpa.com/article/case-study/shopping-guide-lifts-order-value)

[How to Design Products for People Making $2/Day](https://www.fastcoexist.com/1681362/how-to-design-products-for-people-making-2-a-day)

generic / store-brand products
https://hbr.org/2015/04/store-brands-arent-just-about-price

https://iterativepath.wordpress.com/2014/05/11/how-netflix-did-pricing-right/

value-based pricing
https://hbr.org/2016/08/a-quick-guide-to-value-based-pricing

decoy pricing
http://app.mhb.io/e/1bp9a/57

SaaS pricing 2017
http://labs.openviewpartners.com/state-of-saas-pricing/#.WMn5syErJc8

pricing from scratch
http://market-found.com/pricing-product-scratch/

ebook pricing (digital products)
https://blog.asmartbear.com/perfect-pricing.html
https://blog.asmartbear.com/selling-ebook.html
https://arstechnica.com/information-technology/2013/09/how-do-you-put-a-price-on-your-source-code/

raising prices
https://hbr.org/2013/07/want-to-raise-prices-tell-a-be
https://iterativepath.wordpress.com/2014/09/21/price-increase-by-any-other-name-2/

dynamic surge pricing
https://econsultancy.com/blog/61958-five-dynamic-pricing-issues-retailers-should-consider/
https://techcrunch.com/2016/07/18/seatgeek-value-your-ticket/?ncid=rss
http://abovethecrowd.com/2014/03/11/a-deeper-look-at-ubers-dynamic-pricing-model/
https://api.atlasobscura.com/articles/expensive-chocolate-ecuador-toak

https://webdesignledger.com/21-examples-of-pricing-pages-in-web-design/

http://upstreamcommerce.com/blog/2013/01/08/5-valid-reasons-retailers-price-differentiate-geography

http://blog.fractalanalytics.com/pricing-analytics/game-theory-and-strategic-pricing-part-1/
http://blog.fractalanalytics.com/pricing-analytics/game-theory-and-strategic-pricing-part-2/

http://loganvc.com/the-beginning-of-the-end-for-per-user-pricing/

https://blog.kissmetrics.com/psychological-tips-product-pricing/
https://blog.kissmetrics.com/5-psychological-studies/

http://ericsink.com/bos/Product_Pricing.html

http://www.heavybit.com/library/video/harrison-metals-michael-dearing-on-pricing/

http://hbswk.hbs.edu/item/how-our-brain-determines-if-the-product-is-worth-the-price

http://www.practicalecommerce.com/articles/134656-How-to-Sell-High-priced-and-High-quality-Products

https://growthhackers.com/articles/the-psychology-of-pricing-29-strategies-and-tactics

https://conversionxl.com/pricing-strategies-13-articles-you-need-to-read/#.

http://www.priceintelligently.com/blog/bid/163986/A-Complete-Guide-to-Pricing-Strategy

https://iterativepath.wordpress.com/2013/10/27/the-most-beautiful-price-fence/

https://conversionxl.com/category/pricing-strategy/

https://www.groovehq.com/blog/pricing-that-worked

https://medium.com/@tmorkes/pay-what-you-want-the-ultimate-sales-strategy-4030c63c5798#.1b5tcqee4 (pay what you want)

https://news.ycombinator.com/item?id=13840282

### Risk Management

https://blog.ycombinator.com/ycs-series-a-diligence-checklist/

http://tomtunguz.com/the-11-risks-vcs-evaluate/

Due diligence checklist
https://www.dropbox.com/s/s8ab9qrvj0k8mlr/Due%20Diligence%20Checklist.docx?dl=0

https://www.wsj.com/news/articles/SB10001424052702304451104577392270431239772

Make/Buy metric example
https://www.dropbox.com/s/87rf6ngyzgsv9i2/IP%20makebuy%20strategy%20metric.xls?dl=0

Make/Buy metric (IP)
https://www.dropbox.com/s/523j4qbbydeds4w/IP%20makebuy%20strategy%20metric.xls?dl=0

https://gigaom.com/2011/05/21/six-key-principles-of-a-successful-acquisition-strategy-part-1/

https://gigaom.com/2011/05/22/six-key-principles-of-a-successful-acquisition-strategy-part-2/

### Sales Tools

http://www.insightsquared.com/2014/01/how-to-write-a-customer-objection-guide-for-your-sales-team

http://www.insightsquared.com/2012/11/fail-better-with-winloss-analysis/

http://heavyhittersales.typepad.com/heavy_hitter_sales_sales_/2012/03/why-did-i-lose-the-sale-6-win-loss-analysis-questions.html

https://hbr.org/2016/04/5-reasons-good-deals-get-rejected

https://secretpmhandbook.com/the-secrets-of-highly-successful-sales-people-objection-handling/

### Shopping Carts & Checkout

https://blog.kissmetrics.com/1step-checkout-right-way/

https://searchenginewatch.com/sew/how-to/2216887/an-18tip-checklist-to-boost-online-checkouts

http://pud.com/post/87930228516/how-i-increased-conversion-on-my-checkout-form-by

http://www.lunametrics.com/blog/2016/06/30/marketing-channel-attribution-markov-models-r/

http://conversionsciences.com/blog/makes-shoppers-click-infographic/

http://analyzecore.com/2014/10/17/cart-analysis-r/
http://analyzecore.com/2014/10/31/sequence-carts-analysis-sankey/
http://analyzecore.com/2014/12/04/sequence-carts-in-depth-analysis-with-r/
http://analyzecore.com/2015/01/28/sequence-carts-in-depth-analysis-with-r-events/
http://analyzecore.com/2015/02/16/customer-segmentation-lifecycle-grids-with-r/
http://analyzecore.com/2015/02/19/customer-segmentation-lifecycle-grids-clv-and-cac-with-r/
http://analyzecore.com/2015/04/01/cohort-analysis-and-lifecycle-grids-mixed-segmentation-with-r/
http://analyzecore.com/2015/05/03/cohort-analysis-with-heatmap/
http://analyzecore.com/2015/06/23/sales-funnel-visualization-with-r/
http://analyzecore.com/2015/08/27/measuring-business-health-with-delta-lifecycle-grids-and-r/
http://analyzecore.com/2015/12/10/cohort-analysis-retention-rate-visualization-r/
http://analyzecore.com/2016/08/03/attribution-model-r-part-1/ (multichannel)


### Storytelling

elmore leonard's tips:
https://www.theguardian.com/books/2010/feb/24/elmore-leonard-rules-for-writers

https://www.inc.com/alison-davis/the-surprising-way-to-be-more-effective-at-storytelling.html

https://www.vox.com/platform/amp/culture/2018/1/20/16910760/breaking-bad-10th-anniversary-birthday-structure

https://medium.com/the-mission/11-steps-for-branding-with-stories-55b8582601a0#.npekyxkfn

http://www.pixartouchbook.com/blog/2011/5/15/pixar-story-rules-one-version.html

https://www.linkedin.com/pulse/20140429225629-7745996-product-manager-as-storyteller

https://www.noupe.com/wordpress/qards-for-wordpress-you-the-visual-storyteller-91047.html

http://jamesharris.design/periodic/
http://tvtropes.org/pmwiki/pmwiki.php/Main/Tropes

https://medium.com/@alex_godin/good-products-have-features-great-products-have-stories-caedf985569a#.khkbjwbmh

### Strategy

coopetition
https://hbr.org/2014/02/use-co-opetition-to-build-new-lines-of-revenue

prioritization
https://hbr.org/2016/12/how-to-prioritize-your-companys-projects

economy of scale as a service
https://techcrunch.com/2013/04/27/economies-of-scale-as-a-service/

Data as Competitive Advantage
https://codingvc.com/the-value-of-data-part-1-using-data-as-a-competitive-advantage
https://codingvc.com/the-value-of-data-part-2-building-valuable-datasets
https://codingvc.com/the-value-of-data-part-3-data-business-models/

36 Strategems
http://www.warrior-scholar.com/articles/36stratagems.htm

### Tools & Frameworks

* Game Theory
* Art of Profitability

https://robsobers.com/marketing-stacks/

https://segment.com/blog/13-marketing-automation-tools/

https://stackshare.io/marketing-automation

https://hub.uberflip.com/blog/zapier-marketing-automation-hacks

http://www.marketingprofs.com/opinions/2014/25846/the-seven-deadly-sins-of-marketing-automation

APIs:
https://www.programmableweb.com/news/221-marketing-apis-sendgrid-mailchimp-and-eventbrite/2012/07/31

https://www.indicative.com/essential-tools-product-managers/

Categories:

Mindmapping: For organizing concepts in related groupings at the beginning of the ideation process

Flowcharts & Diagrams: For doing high-level process mapping and rough sketching

User Research: For doing qualitative and quantitative research on users and personas to validate concepts

Roadmapping: For organizing and prioritizing product roadmaps, strategy, and releases

Wireframing: For creating low-fidelity product mockups prior to visual design

Prototyping: For creating high-fidelity product prototypes after designing but prior to writing code

Usability Testing: For getting and analyzing user feedback on prototypes and pre-release versions

Agile Project Management: For managing an active product management process in an agile workflow

A/B Testing: For testing different versions of features and copy to optimize your product

Heatmapping & Session Recording: For recording and analyzing individual user sessions and clicking / scrolling

Analytics: For analyzing and optimizing your full customer journey – the most critical part of the process

https://www.airpair.com/analytics/posts/user-analytics-as-your-product-grows

https://www.cbinsights.com/blog/saas-marketing-stack/

https://github.com/cjbarber/ToolsOfTheTrade


### UI (User Interfaces) / UX (User Experience)

https://darkpatterns.org/types-of-dark-pattern

https://lawsofux.com/

http://designinginterfaces.com/patterns/

https://www.pinterest.com/wireframes/great-wireframe-examples/

https://randomwire.com/why-japanese-web-design-is-so-different/

http://thehipperelement.com/post/87574750438/ux-crash-course-user-psychology

tag clouds
http://tagcrowd.com/

twitter cards
https://dev.twitter.com/cards/overview

experience maps
https://medium.com/@wnialloconnor/how-to-build-an-experience-map-5e55b7ee4f32#.v68dluoyo

storyboarding
https://www.smashingmagazine.com/2017/10/storyboarding-ux-design/

user journeys
http://journals.plos.org/ploscompbiol/article?id=10.1371/journal.pcbi.1003833

journey maps
https://conversionxl.com/blog/customer-journey-mapping-examples/

### Virality
